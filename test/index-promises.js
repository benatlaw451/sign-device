'use strict';

var assert = require('chai').assert,
		config = require('./config'),
		deviceGateway = require(config.gateway),
		boundary = require(config.boundary)(deviceGateway);

describe('Boundary Functions with Promises', function() {
	var device,
		unregistered,
		id,
		token;
	beforeEach(function() {
		deviceGateway.setDefault('index', function(data){
			device = data.registered;
			unregistered = data.request;
			id = device.id;
			token = device.token;
		});
	});
	it('newDevice should create a new device', function() {
		return boundary.newDevice(token).then(function(data) {
			assert.isDefined(data, 'Data undefined');
		}, function (error) {
			assert.isUndefined(error, 'Error defined');
		});
	});
	it('newDevice should return error', function() {
		return boundary.newDevice(undefined).then(function(data) {
			assert.isUndefined(data, 'Data defined');
		}, function (error) {
			assert.isDefined(error, 'Error undefined');
		});
	});
	it('registerDevice should register device', function() {
		return boundary.registerDevice(unregistered).then(function(data) {
			assert.isDefined(data, 'Data undefined');
		}, function (error) {
			assert.isUndefined(error, 'Error defined');
		});
	});
	it('registerDevice should return error', function() {
		return boundary.registerDevice(undefined).then(function(data) {
			assert.isUndefined(data, 'Data defined');
		}, function (error) {
			assert.isDefined(error, 'Error undefined');
		});
	});
	it('findDevice should find device by id', function() {
		return boundary.findDevice({id: id}).then(function(data) {
			assert.isDefined(data, 'Data undefined');
		}, function (error) {
			assert.isUndefined(error, 'Error defined');
		});
	});
	it('findDevice should return error', function() {
		return boundary.findDevice(undefined).then(function(data) {
			assert.isUndefined(data, 'Data defined');
		}, function (error) {
			assert.isDefined(error, 'Error undefined');
		});
	});
	it('upsertDevice should upsert device', function() {
		return boundary.upsertDevice(device).then(function(data) {
			assert.isDefined(data, 'Data undefined');
		}, function (error) {
			assert.isUndefined(error, 'Error defined');
		});
	});
	it('upsertDevice should return error', function() {
		return boundary.upsertDevice(undefined).then(function(data) {
			assert.isUndefined(data, 'Data defined');
		}, function (error) {
			assert.isDefined(error, 'Error undefined');
		});
	});
	it('deleteByIdDevice should deleteById device', function() {
		return boundary.deleteByIdDevice(id).then(function(data) {
			assert.isDefined(data, 'Data undefined');
		}, function (error) {
			assert.isUndefined(error, 'Error defined');
		});
	});
	it('deleteByIdDevice should return error', function() {
		return boundary.deleteByIdDevice(undefined).then(function(data) {
			assert.isUndefined(data, 'Data defined');
		}, function (error) {
			assert.isDefined(error, 'Error undefined');
		});
	});
});
