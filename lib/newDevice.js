'use strict';

var shortId = require('shortid'),
		validate = require('./validateDevice');

module.exports = function (gateway) {
	return function (token, cb) {
		var code,
				getDigits,
				callback;

		getDigits = function(num) {
			var result = [];
			for(var i = 0; i < num; i++) {
				result.push(Math.floor(Math.random() * 10));
			};
			return result.join('');
		};

		code = getDigits(3) + '-' + getDigits(3);

		callback = function(error) {
			if (error) {
				return cb(error);
			} else {
				return gateway.save({
						code: code,
						token: token
					},
					cb);
			}
		};

		return validate.token(token, callback);
	};
};
